import re
import time
import string
import random

n = 1000 # Number of iterations
print("################### TEST 1 #######################")

def yandex(test_text):
    a = re.match(r"^[a-zA-Z0-9][a-zA-Z0-9\-\.]{1,18}[a-zA-Z0-9]$", test_text)
    if a:
        return True
    else:
        return None

    
t0 = time.time()
for i in range(n):
    print(yandex(''.join(random.choice(string.ascii_letters + string.digits + "`~!@#$%^&*()_+-=,/<>?{}[]") for _ in range(random.randrange(1,30)))))
t1 = time.time()
total_1_n = t1-t0




print("################### TEST 2 #######################")
def yandex_2(test_text):
    if 1 < len(test_text) < 20:
        if test_text[0].isalpha() or test_text[0].isdigit():
            if test_text[-1].isalpha() or test_text[-1].isdigit():
                iter=0
                for i in test_text[1:-1]:
                    iter+=1
                    if i.isdigit() or i.isalpha() or i in ["-", "."]:
                        if iter == len(test_text[1:-1]):
                            return True
                    else:
                        return None
            else:
                return None
        else:
            return None
    else:
        return None


t0 = time.time()
for i in range(n):
    print(yandex_2(''.join(random.choice(string.ascii_letters + string.digits + "`~!@#$%^&*()_+-=,/<>?{}[]") for _ in range(random.randrange(1,30)))))
t1 = time.time()
total_2_n = t1-t0

print("################ RESULTS #####################")

print("Total time of the Test 1: ", total_1_n)
print("Total time ot the Test 2: ", total_2_n)
